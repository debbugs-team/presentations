#!/usr/bin/perl

use warnings;
use strict;

use SQL::Translator;
use Debbugs::DB;

my $s = Debbugs::DB->connect('dbi:Pg:service=debbugs');

my $trans = SQL::Translator->new(
    parser        => 'SQL::Translator::Parser::DBIx::Class',
    parser_args   => { dbic_schema => $s },
    producer      => 'Diagram',
    producer_args => {
        out_file         => $ARGV[0],
        show_constraints => 1,
        show_datatypes   => 1,
        show_sizes       => 1,
        show_fk_only     => 0,
    } );
$trans->translate();
