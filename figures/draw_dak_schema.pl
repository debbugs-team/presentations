#!/usr/bin/perl

use warnings;
use strict;

use SQL::Translator;
use IO::File;

my $fh = IO::File->new($ARGV[0]);

my $keep = 0;
my $file = '';
while (<$fh>) {
    if (/CREATE\s*TABLE/i) {
        $keep = 1;
        $file .= $_;
    }
    elsif (/^\s*\);\s*$/) {
        $file .= $_;
        $keep = 0;
    } elsif ($keep) {
        $file .= $_;
    } elsif (/CREATE\s*(UNIQUE\s*)?INDEX/i) {
        $file .= $_;
    }
}

my $trans = SQL::Translator->new(
    parser        => 'SQL::Translator::Parser::PostgreSQL',
    producer      => 'Diagram',
    producer_args => {
        out_file         => $ARGV[1],
        show_constraints => 1,
        show_datatypes   => 1,
        show_sizes       => 1,
        show_fk_only     => 0,
    } );
$trans->translate(\$file);
