# Debbugs: 22 years of bugs

The Debian Bug Tracking system Debbugs is rapidly approaching 1
million bugs and 90,000 unique correspondents, and is still central to
the release and development process of Debian. We will examine the
history and statistics of bug reporting in Debian, discuss the changes
being made to make Debbugs faster and help maintainers cope with
additional bugs. We will also explain how developers can contribute to
Debbugs and join the Debbugs development team.
